<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OfficerLoginSignUpController;
use App\Http\Controllers\OfficerDashboardController;
use App\Http\Controllers\OfficerStudentListController;
use App\Http\Controllers\OfficerStudentReportController;
use App\Http\Controllers\OfficerEventAttendanceController;
use App\Http\Controllers\OfficerEventSelectedController;
use App\Http\Controllers\OfficerScanController;
use App\Http\Controllers\OfficerEventController;
use App\Http\Controllers\OfficerAccountManagementController;
use App\Http\Controllers\OfficerAnnouncementController;

use App\Http\Controllers\StudentLoginSignUpController;
use App\Http\Controllers\StudentSignUpSupportController;
use App\Http\Controllers\StudentDashboardController;
use App\Http\Controllers\StudentQrcodeController;
use App\Http\Controllers\StudentAttendanceReportController;
use App\Http\Controllers\StudentEventsController;
use App\Http\Controllers\StudentAccountManagementController;
use App\Http\Controllers\StudentUpdateProfileController;

use App\Http\Controllers\AttendanceController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    /**This line will redirect to officer sign in page. */
    //return redirect('/officer');
    /**This line will redirect to student sign in page. */
    return redirect('/student');
});

Route::get('/student', function () {
    return redirect('/student-signin');
});

Route::get('/student-signin', function () {
    if(session()->has('sid')){
        return redirect()->route(session('student-location').'.index');
    }else{
        return view('credentials.student-sign-in');     
    }
});

Route::get('/student-logout', function () {
    session()->forget('sid');
    session()->forget('student-location');
    return redirect('/student-signin');
});

Route::get('/officer', function () {
    return redirect('/officer-signin');
});

Route::get('/officer-signin', function () {
    if(session()->has('oid')){
        return redirect()->route(session('officer-location').'.index');
    }else{
        return view('credentials.officer-sign-in');
    }
});

Route::get('/officer-logout', function () {
    session()->forget('oid');
    session()->forget('officer-location');
    return redirect('/officer-signin');
});

Route::get('/officer-sl', function () {
    $ys = ["All","All"];
    session()->put('ys', $ys);
    return redirect('/officer-studentlist');
});

Route::get('/officer-ea', function () {
    $eid = "";
    session()->put('eid', $eid);
    return redirect('/officer-eventattendance');
});
/*
Route::get('/officer-eventattendance/officer-ea', function () {
    $eid = "";
    session()->put('eid', $eid);
    return redirect('/officer-eventattendance');
});
*/
Route::resource('/officer-signup', OfficerLoginSignUpController::class);
Route::resource('/officer-dashboard', OfficerDashboardController::class);
Route::resource('/officer-studentlist', OfficerStudentListController::class);
Route::resource('/officer-studentreport', OfficerStudentReportController::class);
Route::resource('/officer-eventattendance', OfficerEventAttendanceController::class);
Route::resource('/officer-eventattendance-selected', OfficerEventSelectedController::class);
Route::resource('/officer-scan', OfficerScanController::class);
Route::resource('/officer-event', OfficerEventController::class);
Route::resource('/officer-accountmanagement', OfficerAccountManagementController::class);
Route::resource('/officer-announcement', OfficerAnnouncementController::class);

Route::resource('/student-signup', StudentLoginSignUpController::class);
Route::resource('/student-signup-support', StudentSignUpSupportController::class);
Route::resource('/student-dashboard', StudentDashboardController::class);
Route::resource('/student-qrcode', StudentQrcodeController::class);
Route::resource('/student-attendancereport', StudentAttendanceReportController::class);
Route::resource('/student-event', StudentEventsController::class);
Route::resource('/student-accountmanagement', StudentAccountManagementController::class);
Route::resource('/student-updateprofile', StudentUpdateProfileController::class);

Route::resource('/attendance', AttendanceController::class);

