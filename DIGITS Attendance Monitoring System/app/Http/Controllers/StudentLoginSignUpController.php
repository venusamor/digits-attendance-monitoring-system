<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StudentInfo;
use App\Models\StudentCred;
use App\Models\Year;
use App\Models\Section;

class StudentLoginSignUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('sid')){
            return redirect()->route(session('student-location').'.index');
        }else{
            $year = Year::all();
            $section = Section::all();
            return view('credentials.student-sign-up',compact('year','section'));        
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $cred = StudentCred::where('username','=',$request->username)->get();
        $i = 0;
        $i = count($cred);

        if($i > 0){
            if($cred[0]->password == md5($request->password)){
                session()->put('sid', $cred[0]->sid);
                $message = array("welcomeMessage","Hello ");
                session()->put('student-message',$message);
                return redirect()->route('student-dashboard.index');
            }else{
                $message = array("warningMessage","Password did not match.");
                session()->put('student-message',$message);
                return redirect('/student-signin');
            }
        }else{
            $message = array("warningMessage","Password did not match.");
            session()->put('student-message',$message);
            return redirect('/student-signin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->password != $request->repassword){
            $message = array("warningMessage","Password did not match.");
            session()->put('student-message',$message);

            return redirect()->route(session('student-location').'.index');
        }else{
            $credtemp = StudentCred::where('sid','=',$id)->get();
            $cred = StudentCred::find($credtemp[0]->id);
            $cred->password = md5($request->password);
            $cred->save();

            $message = array("successMessage","Password saved.");
            session()->put('student-message',$message);

            return redirect()->route(session('student-location').'.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
