<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OfficerInfo;
use App\Models\OfficerEvent;
use App\Models\StudentInfo;
use App\Models\Position;
use App\Models\Section;
use App\Models\Year;
use App\Models\OfficerAnnouncement;

class OfficerDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('oid')){
            session()->put('officer-location','officer-dashboard');

            $oid = session('oid');
            $info = OfficerInfo::where('oid','=',$oid)->get();
            $student = StudentInfo::all();
            $officer = OfficerInfo::all();
            $event = OfficerEvent::all();
            $position = Position::all();
            $year = Year::all();
            $section = Section::all();
            $announcement = OfficerAnnouncement::all();

            $s = 0;
            $s = count($student);
            $s = $s + count($officer);

            $o = 0;
            $o = count($officer);

            $e = 0;
            $e = count($event);

            return view('officer.dashboard',compact('info','student','position','year','section','announcement','s','o','e'));
        }else{
            return redirect('/officer-signin');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
