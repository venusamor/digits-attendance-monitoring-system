<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StudentInfo;
use App\Models\OfficerInfo;
use App\Models\OfficerEvent;
use App\Models\StudentAttendance;
use App\Models\Year;
use App\Models\Section;

class StudentEventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('sid')){
            session()->put('student-location','student-event');

            $sid = session('sid');
            $info = StudentInfo::where('sid','=',$sid)->get();
            $studentattendance = StudentAttendance::where('sid','=',$sid)->get();
            $student = StudentInfo::all();
            $officer = OfficerInfo::all();
            $event = OfficerEvent::all();
            $year = Year::all();
            $section = Section::all();
            return view('student.event',compact('info','event','year','section','studentattendance'));     
        }else{
            return redirect('/student-signin');
        }
        session()->put('student-location','student-event');

        $sid = session('sid');
        $info = StudentInfo::where('sid','=',$sid)->get();
        $studentattendance = StudentAttendance::where('sid','=',$sid)->get();
        $student = StudentInfo::all();
        $officer = OfficerInfo::all();
        $event = OfficerEvent::all();
        $year = Year::all();
        $section = Section::all();
        return view('student.event',compact('info','event','year','section','studentattendance')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
