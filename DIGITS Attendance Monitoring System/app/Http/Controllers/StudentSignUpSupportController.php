<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StudentInfo;
use App\Models\StudentCred;

class StudentSignUpSupportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $qrcode = 'DIGITS.'.$request->stuid.'.'.$request->sid.'.png';
        if($request->password != $request->repassword){
            $destination = "addons/assets/root/qrcodes/";
            unlink($destination. $qrcode);

            $message = array("warningMessage","Password did not match.");
            session()->put('student-message',$message);

            return redirect()->route('student-signup.index');
        }else{
            /**
             * Insert student info
             */
            $info = new StudentInfo;
            $info->sid = $request->sid;
            $info->stuid = $request->stuid;
            $info->lname = $request->lname;
            $info->fname = $request->fname;
            $info->mi = $request->mi;
            $info->age = $request->age;
            $info->birthdate = $request->bday;
            $info->address = $request->address;
            $info->email = $request->email;
            $info->year = $request->year;
            $info->section = $request->section;
            $info->position = "Student";
            $info->qrcode = $qrcode;
            $info->profile = "default.png";
            $info->save();

            /**
             * Insert student credentials
             */
            $cred = new StudentCred;
            $cred->sid = $request->sid;
            $cred->username = $request->stuid;
            $cred->password = md5($request->password);
            $cred->save();

            $message = array("successMessage","You can now sign in to your account.");
            session()->put('student-message',$message);

            return redirect('/student-signin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
