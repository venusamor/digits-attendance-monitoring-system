<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OfficerInfo;
use App\Models\OfficerEvent;
use App\Models\Position;
use App\Models\Section;
use App\Models\Year;

class OfficerEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('oid')){
            session()->put('officer-location','officer-event');

            $oid = session('oid');
            $info = OfficerInfo::where('oid','=',$oid)->get();
            $event = OfficerEvent::all()->reverse();
            $position = Position::all();
            $year = Year::all();
            $section = Section::all();

            return view('officer.school-event',compact('info','event','position','year','section'));
        }else{
            return redirect('/officer-signin');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new OfficerEvent;
        $event->eid = uniqid();
        $event->name = $request->name;
        $event->venue = $request->venue;
        $event->date = $request->date;
        $event->save();
        $message = array("successMessage","Event created.");
        session()->put('officer-message',$message);
        return redirect()->route('officer-event.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = OfficerEvent::find($id);
        $event->delete();

        return redirect()->route('officer-event.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = OfficerEvent::find($id);
        $event->name = $request->name;
        $event->venue = $request->venue;
        $event->date = $request->date;
        $event->save();
        $message = array("successMessage","Changes saved.");
        session()->put('officer-message',$message);
        return redirect()->route('officer-event.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
