<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Section;
use App\Models\Year;
use App\Models\Position;

use App\Models\OfficerInfo;
use App\Models\OfficerCred;

class OfficerLoginSignUpController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('oid')){
            return redirect()->route(session('officer-location').'.index');
        }else{
            $year = Year::all();
            $section = Section::all();
            $position = Position::all();
            return view('credentials.officer-sign-up', compact('year','section','position'));    
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->password != $request->repassword){
            $message = array("warningMessage","Password did not match.");
            session()->put('officer-message',$message);

            return redirect()->route('officer-signup.index');
        }else{
            $oid = uniqid();

            $info = new OfficerInfo;
            $info->oid = $oid;
            $info->stuid = $request->stuid;
            $info->lname = $request->lname;
            $info->fname = $request->fname;
            $info->mi = $request->mi;
            $info->age = $request->age ;
            $info->birthdate = $request->bday;
            $info->address = $request->address;
            $info->email = $request->email;
            $info->year = $request->year;
            $info->section = $request->section;
            $info->position = $request->position;
            $info->save();

            $cred = new OfficerCred;
            $cred->oid = $oid;
            $cred->username = $request->stuid;
            $cred->password = md5($request->password);
            $cred->save();

            $message = array("successMessage","You can now sign in to your account.");
            session()->put('officer-message',$message);

            return redirect('/officer-signin');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $cred = OfficerCred::where('username','=',$request->username)->get();
        $i = 0;
        $i = count($cred);

        if($i > 0){
            if($cred[0]->password == md5($request->password)){
                session()->put('oid', $cred[0]->oid);
                $message = array("welcomeMessage","Hello ");
                session()->put('officer-message',$message);
                return redirect()->route('officer-dashboard.index');
            }else{
                $message = array("warningMessage","Username or password is incorrect.");
                session()->put('officer-message',$message);
                return redirect('/officer-signin');
            }
        }else{
            $message = array("warningMessage","Username or password is incorrect.");
            session()->put('officer-message',$message);
            return redirect('/officer-signin');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->password != $request->repassword){
            $message = array("warningMessage","Password did not match.");
            session()->put('officer-message',$message);

            return redirect()->route(session('officer-location').'.index');
        }else{
            $credtemp = OfficerCred::where('oid','=',$id)->get();
            $cred = OfficerCred::find($credtemp[0]->id);
            $cred->password = md5($request->password);
            $cred->save();

            $message = array("successMessage","Password saved.");
            session()->put('officer-message',$message);

            return redirect()->route(session('officer-location').'.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
