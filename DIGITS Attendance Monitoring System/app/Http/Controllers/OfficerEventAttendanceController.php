<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OfficerInfo;
use App\Models\OfficerEvent;
use App\Models\StudentInfo;
use App\Models\StudentAttendance;
use App\Models\Position;
use App\Models\Section;
use App\Models\Year;


class OfficerEventAttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('oid')){
            session()->put('officer-location','officer-eventattendance');

            $oid = session('oid');
            $info = OfficerInfo::where('oid','=',$oid)->get();
            $student = StudentInfo::all();
            $event = OfficerEvent::all()->reverse();
            $position = Position::all();
            $year = Year::all();
            $section = Section::all();
            $i = 0;
            $i = count($event);
            if($i > 0){
                $i--;
                $eventselected = OfficerEvent::where('eid','=',$event[$i]->eid)->get();
                $studentattendance = StudentAttendance::where('eid','=',$event[$i]->eid)->get(); 
                session()->put('event-name', $eventselected[0]->name);  
            }else{
                $eventselected = OfficerEvent::all();
                $studentattendance = StudentAttendance::all();
                session()->put('event-name', '');
            }
            
            
            
            return view('officer.event-attendance',compact('info','event','position','year','section','eventselected','studentattendance','student'));
        }else{
            return redirect('/officer-signin');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('officer-eventattendance-selected.index',compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
