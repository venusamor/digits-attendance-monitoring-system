<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StudentInfo;
use App\Models\OfficerInfo;
use App\Models\OfficerEvent;
use App\Models\StudentAttendance;
use App\Models\Year;
use App\Models\Section;
use App\Models\OfficerAnnouncement;

class StudentDashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('sid')){
            session()->put('student-location','student-dashboard');

            $sid = session('sid');
            $info = StudentInfo::where('sid','=',$sid)->get();
            $studentattendance = StudentAttendance::where('sid','=',$sid)->get();
            $student = StudentInfo::all();
            $officer = OfficerInfo::all();
            $event = OfficerEvent::all();
            $year = Year::all();
            $section = Section::all();
            $announcement = OfficerAnnouncement::all();
            $s = 0;
            $s = count($student);
            $s = $s + count($officer);

            $o = 0;
            $o = count($officer);

            $e = 0;
            $e = count($event);

            $sanction = 0;

            $sacount = count($studentattendance);
            $secount = count($event);
            $sanction = $secount - $sacount;

            foreach($studentattendance as $sa){
                if($sa->mout == ""){
                    $sanction = $sanction + 0.25;
                }
                if($sa->ain == ""){
                    $sanction = $sanction + 0.25;
                }
                if($sa->aout == ""){
                    $sanction = $sanction + 0.25;
                }
            }
            return view('student.dashboard',compact('info','year','section','s','e','sanction','announcement'));    
        }else{
            return redirect('/student-signin');
        }
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
