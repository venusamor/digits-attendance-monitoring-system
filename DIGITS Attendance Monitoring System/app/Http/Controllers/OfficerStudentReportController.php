<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StudentInfo;
use App\Models\StudentAttendance;
use App\Models\OfficerEvent;
use App\Models\Year;
use App\Models\Section;

class OfficerStudentReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $infotemp = StudentInfo::find($request->id);
        $sid = $infotemp->sid;
        $info = StudentInfo::where('sid','=',$sid)->get();
        $studentattendance = StudentAttendance::where('sid','=',$sid)->get();
        $event = OfficerEvent::all();
        $year = Year::all();
        $section = Section::all();

        $sanction = 0;

        $sacount = count($studentattendance);
        $secount = count($event);
        $sanction = $secount - $sacount;

        foreach($studentattendance as $sa){
            if($sa->mout == ""){
                $sanction = $sanction + 0.25;
            }
            if($sa->ain == ""){
                $sanction = $sanction + 0.25;
            }
            if($sa->aout == ""){
                $sanction = $sanction + 0.25;
            }
        }
        return view('officer.studentreport',compact('info','event','year','section','studentattendance','sanction'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $stuid = $request->stuid;
        $info = StudentInfo::where('stuid','=',$stuid)->get();
        $i = 0;
        $i = count($info);
        if($i > 0){
            $id = $info[0]->id;
            return redirect()->route('officer-studentreport.index',compact('id'));
        }else{
            $message = array("warningMessage","Didn't find any match.");
            session()->put('officer-message',$message);
            return redirect()->route('officer-studentlist.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->route('officer-studentreport.index',compact('id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
