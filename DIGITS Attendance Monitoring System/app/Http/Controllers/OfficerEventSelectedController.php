<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OfficerInfo;
use App\Models\OfficerEvent;
use App\Models\StudentInfo;
use App\Models\StudentAttendance;
use App\Models\Position;
use App\Models\Section;
use App\Models\Year;

class OfficerEventSelectedController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $id = $request->id;
        $oid = session('oid');
        $info = OfficerInfo::where('oid','=',$oid)->get();
        $student = StudentInfo::all();
        $event = OfficerEvent::all()->reverse();
        $position = Position::all();
        $year = Year::all();
        $section = Section::all();
        $i = 0;
        $i = count($event);
        $i--;
        $eventtemp = OfficerEvent::find($id);
        $eventselected = OfficerEvent::where('eid','=',$eventtemp->eid)->get();
        $studentattendance = StudentAttendance::where('eid','=',$eventtemp->eid)->get();
        session()->put('event-name', $eventselected[0]->name); 
        return view('officer.event-attendance',compact('info','event','position','year','section','eventselected','studentattendance','student'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
