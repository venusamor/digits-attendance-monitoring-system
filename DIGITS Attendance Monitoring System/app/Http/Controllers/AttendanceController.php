<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StudentAttendance;


class AttendanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $sid = $request->sid;
        $date = $request->d;
        $time = $request->t;
        $ampm = $request->ampm;
        $eid = $request->eid;
        $timetemp = explode(':', $time);

        $tempid = 0;
        $tempmin = "";
        $tempmout = "";
        $tempain = "";
        $tempaout = "";
        $allattendance = StudentAttendance::where('eid','=',$eid)->get();
        $aacount = 0;
        $aacount = count($allattendance);
        if($aacount == 0){
            //insert fresh
            $nat = new StudentAttendance;
            $nat->eid = $eid;
            $nat->sid = $sid;
            $nat->min = $time;
            $nat->mout = "";
            $nat->ain = "";
            $nat->aout = "";
            $nat->save();
            $a = array("success","Signed In");
            return response()->json($a);
        }else{
            //find match
            foreach($allattendance as $aa){
                //if match save as temp
                if($aa->sid == $sid){
                    $tempid = $aa->id;
                    $tempmin = $aa->min;
                    $tempmout = $aa->mout;
                    $tempain = $aa->ain;
                    $tempaout = $aa->aout;
                }
            }
        }

        if($tempid == 0){
            //insert fresh
            $nat = new StudentAttendance;
            $nat->eid = $eid;
            $nat->sid = $sid;
            $nat->min = $time;
            $nat->mout = "";
            $nat->ain = "";
            $nat->aout = "";
            $nat->save();
            $a = array("success","Signed In Parin");
            return response()->json($a);
        }else{
            //find attendance and update
            $attendanceupdate = StudentAttendance::find($tempid);
            //if morning time out isn't blank
            if($tempmout != ""){
                //if arternoon time in isn't blank
                if($tempain != ""){
                    //if arternoon time out isn't blank
                    if($tempaout != ""){
                        $a = array("warning","Multiple Scan");
                        return response()->json($a); 
                    //if arternoon time out is blank
                    }else{
                        $tempaintemp = explode(':', $tempain);
                        if($tempaintemp[0] == $timetemp[0]){
                            $a = array("warning","Multiple Scan");
                            return response()->json($a); 
                        }else{
                            $attendanceupdate->aout = $time;
                            $attendanceupdate->save();
                            $a = array("info","Signed Out");
                            return response()->json($a);       
                        }
                        $attendanceupdate->aout = $time;
                        $attendanceupdate->save();
                        $a = array("info","Signed Out");
                        return response()->json($a);
                    }
                //if afternoon time in is blank
                }else{
                    $tempmouttemp = explode(':', $tempmout);
                    if($tempmouttemp[0] == $timetemp[0]){
                        $a = array("warning","Multiple Scan");
                        return response()->json($a); 
                    }else{
                        $attendanceupdate->ain = $time;
                        $attendanceupdate->save();
                        $a = array("success","Signed In");
                        return response()->json($a);    
                    }
                }
            //if morning time out is blank
            }else{
                $tempmintemp = explode(':', $tempmin);
                if($tempmintemp[0] == $timetemp[0]){
                    $a = array("warning","Multiple Scan");
                    return response()->json($a); 
                }else{
                    $attendanceupdate->mout = $time;
                    $attendanceupdate->save();
                    $a = array("info","Signed Out");
                    return response()->json($a);    
                }
                
            }
            $a = array("success","Signed Out");
            return response()->json($attendanceupdate);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
