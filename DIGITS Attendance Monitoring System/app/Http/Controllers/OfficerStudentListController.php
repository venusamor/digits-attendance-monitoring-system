<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\OfficerInfo;
use App\Models\StudentInfo;
use App\Models\Position;
use App\Models\Section;
use App\Models\Year;

class OfficerStudentListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('oid')){
            session()->put('officer-location','officer-studentlist');

            $oid = session('oid');
            $info = OfficerInfo::where('oid','=',$oid)->get();
            if(session('ys')[0] == "All"){
                $student = StudentInfo::all();
            }else{
                $student = StudentInfo::where('year','=',session('ys')[0])->get();
            }
            $position = Position::all();
            $year = Year::all();
            $section = Section::all();
            return view('officer.student-list',compact('info','student','position','year','section'));
        }else{
            return redirect('/officer-signin');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $year = $request->year;
        $section = $request->section;
        $ys = [$year,$section];
        session()->put('ys', $ys);

        return redirect()->route('officer-studentlist.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
