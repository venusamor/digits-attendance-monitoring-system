<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentCred extends Model
{
    use HasFactory;
    protected $table = "studentcred";
    protected $fillable = [
        'sid',
        'username',
        'password',
    ];
}
