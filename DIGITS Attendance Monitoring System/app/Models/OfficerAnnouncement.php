<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfficerAnnouncement extends Model
{
    use HasFactory;
    protected $table = "officerannouncement";
    protected $fillable = [
        'announcer',
        'announcement',
        'date',
    ];
}
