<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfficerEvent extends Model
{
    use HasFactory;
    protected $table = "officerevent";
    protected $fillable = [
        'eid',
        'name',
        'venue',
        'date',
    ];
}
