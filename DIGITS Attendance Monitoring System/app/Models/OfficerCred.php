<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfficerCred extends Model
{
    use HasFactory;
    protected $table = "officercred";
    protected $fillable = [
        'oid',
        'username',
        'password',
    ];
}
