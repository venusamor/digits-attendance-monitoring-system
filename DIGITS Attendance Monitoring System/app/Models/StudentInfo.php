<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentInfo extends Model
{
    use HasFactory;
    protected $table = "studentinfo";
    protected $fillable = [
        'sid',
        'stuid',
        'lname',
        'fname',
        'mi',
        'age',
        'birthdate',
        'address',
        'email',
        'year',
        'section',
        'position',
        'qrcode',
        'profile',
    ];
}
