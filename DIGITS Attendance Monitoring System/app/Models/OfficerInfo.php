<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OfficerInfo extends Model
{
    use HasFactory;
    protected $table = "officerinfo";
    protected $fillable = [
        'oid',
        'stuid',
        'lname',
        'fname',
        'mi',
        'age',
        'birthdate',
        'address',
        'email',
        'year',
        'section',
        'position',
        'qrcode',
        'profile',
    ];
}
