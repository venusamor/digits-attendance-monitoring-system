<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentAttendance extends Model
{
    use HasFactory;
    protected $table = "studentattendance";
    protected $fillable = [
        'eid',
        'sid',
        'min',
        'mout',
        'ain',
        'aout',
    ];
}
