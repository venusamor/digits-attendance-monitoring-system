<div class="sidebar-container">
    <div class="innitials">
        <div class="modal-header">
            <h5><?php echo $info[0]->lname[0].$info[0]->fname[0];?></h5>
            <div class="account-details">
                <h6><?php echo $info[0]->lname.', '.$info[0]->fname.' '.$info[0]->mi.'.';?></h6>
                <h6><?php echo $info[0]->email;?></h6>
            </div>
        </div>
    </div>
    <a href="student-dashboard" class="<?php if(session('student-location') == 'student-dashboard'){ echo 'isActive';}?>"><span class="fa fa-tachometer"></span> Dashboard</a>
    <a href="student-qrcode"><span class="fa fa-qrcode"></span> QR Code</a>
    <a href="student-attendancereport" class="<?php if(session('student-location') == 'student-attendancereport'){ echo 'isActive';}?>"><span class="fa fa-list"></span> Attendance Report</a>
    <a href="student-event" class="<?php if(session('student-location') == 'student-event'){ echo 'isActive';}?>"><span class="fa fa-flag"></span> School Events</a>
    <a href="#" data-toggle="modal" data-target="#accountmanagement"><span class="fa fa-gears"></span> Account Management</a>
    <a href="#" data-toggle="modal" data-target="#profilepicture"><span class="fa fa-image"></span> Changes Profile Picture</a>
    <a href="student-logout"><span class="fa fa-power-off"></span> Logout</a>
</div>