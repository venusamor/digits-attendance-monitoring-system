<div class="sidebar-container">
    <div class="innitials">
        <div class="modal-header">
            <h5><?php echo $info[0]->lname[0].$info[0]->fname[0];?></h5>
            <div class="account-details">
                <h6><?php echo $info[0]->lname.', '.$info[0]->fname.' '.$info[0]->mi.'.';?></h6>
                <h6><?php echo $info[0]->email;?></h6>
            </div>
        </div>
    </div>
    <a href="officer-dashboard" class="<?php if(session('officer-location') == 'officer-dashboard'){ echo 'isActive';}?>"><span class="fa fa-tachometer"></span> Dashboard</a>
    <a href="officer-sl" class="<?php if(session('officer-location') == 'officer-studentlist'){ echo 'isActive';}?>"><span class="fa fa-users"></span> Students Informations</a>
    <a href="officer-ea" class="<?php if(session('officer-location') == 'officer-eventattendance'){ echo 'isActive';}?>"><span class="fa fa-calendar"></span> Event Attendance</a>
    <a href="officer-event" class="<?php if(session('officer-location') == 'officer-event'){ echo 'isActive';}?>"><span class="fa fa-flag"></span> School Events</a>
    <a href="#" data-toggle="modal" data-target="#accountmanagement"><span class="fa fa-gears"></span> Account Management</a>
    <a href="officer-logout"><span class="fa fa-power-off"></span> Logout</a>
</div>