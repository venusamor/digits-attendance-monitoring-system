<?php
    $senderID = $_GET['sen'];
    $recipientID = $_GET['rec'];
    $command = $_GET['command'];
    $filetype = "file";
    $destination = "../assets/root/mfiles/";
    $file = $_FILES['new_file'];

    $filename_primary = $_FILES['new_file']['name'];
    $filesize = $_FILES['new_file']['size'];
    $filetmp = $_FILES['new_file']['tmp_name'];

    $file_ext_primary = explode('.', $filename_primary);
    $file_ext_secondary = strtolower(end($file_ext_primary));

    $filename_secondary = uniqid('', true);
    $filename_secondary = $filename_secondary.".".$file_ext_secondary;

    $imageFile = array('png','jpg','jpeg','img','ico');
    $musicFile = array('mp3','wma','ape','ogg','fla','aac','m4a','ac3','wav','wv');
    $videoFile = array('mp4','avi','mkv','mov','mpeg','webm','3gp');
    $xlsxfile = array('xls','xlsx');
    $docxFile = array('doc','docx');
    $pptxFile = array('ppt','pptx');
    $pdfFile = array('pdf');
    $compressFile = array('zip','rar','bin','iso');

    if(in_array($file_ext_secondary, $imageFile)){
        $filetype = "fa fa-file-photo-o";
    }else if(in_array($file_ext_secondary, $musicFile)){
        $filetype = "fa fa-file-audio-o";
    }else if(in_array($file_ext_secondary, $videoFile)){
        $filetype = "fa fa-file-video-o";
    }else if(in_array($file_ext_secondary, $xlsxfile)){
        $filetype = "fa fa-file-excel-o";
    }else if(in_array($file_ext_secondary, $docxFile)){
        $filetype = "fa fa-file-word-o";
    }else if(in_array($file_ext_secondary, $pptxFile)){
        $filetype = "fa fa-file-powerpoint-o";
    }else if(in_array($file_ext_secondary, $pdfFile)){
        $filetype = "fa fa-file-pdf-o";
    }else if(in_array($file_ext_secondary, $compressFile)){
        $filetype = "fa fa-file-zip-o";
    }else{
        $filetype = "fa fa-file";
    }
    $arraycount = count($file_ext_primary);
    $filenametemp = "";
    if($arraycount > 2){
        $i = 0;
        while($i < $arraycount-1){
            if($filenametemp == ""){
                $filenametemp = $file_ext_primary[$i];
            }else{
                $filenametemp = $filenametemp.'.'.$file_ext_primary[$i];
            }
            $i++;
        }
    }else{
        $filenametemp = $file_ext_primary[0];
    }

    if($filesize < 41943040){
        $filenameDisplay = $filenametemp.'.'.$file_ext_secondary;
        $dest = $destination.$filename_secondary;
        move_uploaded_file($filetmp, $dest);
        echo $filenameDisplay;
        if($command == "admin"){
            header("location: ../../admin/upload-file?sen=$senderID&rec=$recipientID&name=$filenameDisplay&type=$filetype&file=$filename_secondary");
        }else if($command == "user"){
            header("location: ../../officer/upload-file?sen=$senderID&rec=$recipientID&name=$filenameDisplay&type=$filetype&file=$filename_secondary");
        }else{
            echo "Opppss something went wrong try again later...";
        }
        
    }else{
        echo "Your file is too big";
    }
?>