<?php
    $currentProfile = $_GET['profile'];
    $id = $_GET['id'];
    $destination = "../assets/root/profiles/";
    $file = $_FILES['new_profile'];

    $filename_primary = $_FILES['new_profile']['name'];
    $filetmp = $_FILES['new_profile']['tmp_name'];

    $file_ext_primary = explode('.', $filename_primary);
    $file_ext_secondary = strtolower(end($file_ext_primary));

    $filename_secondary = uniqid('', true);
    $filename_secondary = $filename_secondary.".".$file_ext_secondary;

    $allowed = array('png','jpg','jpeg','img');

    //echo $file_ext_secondary;
    //echo "<br>";
    //echo $currentProfile;

    if(in_array($file_ext_secondary, $allowed)){
        if($currentProfile != "default.png"){
            unlink($destination. $currentProfile);
        }

        $dest = $destination.$filename_secondary;
        move_uploaded_file($filetmp, $dest);
        header("location: ../../student-updateprofile?id=$id&profile=$filename_secondary");
    }else{
        header("location: ../../student-updateprofile?id=$id&profile=filetypeerror");
    }
    
?>