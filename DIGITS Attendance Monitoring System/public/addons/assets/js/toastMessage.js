function warningMessage(data){
    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
    }
    toastr.warning(data);
}
function successMessage(data){
    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
    }
    toastr.success(data);
}
function infoMessage(data){
    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
    }
    toastr.info(data);
}
function errorMessage(data){
    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
    }
    toastr.error(data);
}
function welcomeMessage(data){
    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
    }
    toastr.info(data);
}