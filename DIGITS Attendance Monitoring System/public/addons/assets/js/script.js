function displayname(input,_this) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            if(input.files[0]['size'] > 40000000){
                _this.siblings('label').html("File size too large.")
                toastr.warning("File size too large, max 40mb.");
            }
            else{
                _this.siblings('label').html(input.files[0]['name'])
            }
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function rateChange(data){
    var i = 1;
    var a = document.getElementById("rateDescription");
    var b = document.getElementById("rateAmmount");
    a.remove();
    b.insertAdjacentHTML('afterend', '<div id="rateDescription" class="mt-3"><label>Description</label></div>');
    var a = document.getElementById("rateDescription");
    while(i <= data){
        a.insertAdjacentHTML('beforeend', '<div class="btn-group col"><button disabled class="btn btn-dark btn-sm mb-2">'+i+'</button><input required class="mb-2 form-control" type="text" name="'+i+'"></div>');
        i++;
    }
}
function printSheet(printArea){
    var a = document.body;
    a.style = "background-color: white;";
    var table = document.getElementById('table');
    table.classList.remove("table-hover");
    var rowtitle = document.getElementById('rowtitle');
    rowtitle.style = "display: none";
    var prttitle = document.getElementById('prttitle');
    prttitle.style = "display: flex;";
    var rowDataCount = document.getElementById('rowDataCount').value;
    var i = 1;
    while(i <= rowDataCount){
        var rowdata = document.getElementById(i);
        rowdata.style = "display: none";
        i++;
    }
    
    var printContents = document.getElementById(printArea).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;

    var a = document.body;
    a.style = "background-color: #e9e9e9;";
    var table = document.getElementById('table');
    table.classList.add("table-hover");
    var rowtitle = document.getElementById('rowtitle');
    rowtitle.style = "display: ;";
    var prttitle = document.getElementById('prttitle');
    prttitle.style = "display: none;";

    i = 1;
    while(i <= rowDataCount){
        var rowdata = document.getElementById(i);
        rowdata.style = "display: ; text-align: center";
        i++;
    }
}

function printCard(data){
    console.log(data)
    var a = document.body;
    a.style = "background-color: white;";
    var printContents = document.getElementById(data).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    var a = document.body;
    a.style = "background-color: #e9e9e9;";
}

function printSlip(data){
    var a = document.body;
    a.style = "background-color: white;";
    var printContents = document.getElementById(data).innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
    var a = document.body;
    a.style = "background-color: #e9e9e9;";
}

function next(i){
    if(i == 2){
        var stuid = document.getElementById("stuid").value;
        if(!stuid){
            toastr.warning("Schood ID is required.");
        }
        else{
            connext(i);
        }
    }
    if(i == 3){
        var fname = document.getElementById("fname").value;
        var lname = document.getElementById("lname").value;
        var mi = document.getElementById("mi").value;
        var address = document.getElementById("address").value;
        if(!fname || !lname || !mi){
            toastr.warning("Complete name is required.");
        }
        else{
            if(!address){
                toastr.warning("Address is required.");
            }
            else{
                connext(i);
            }
        }
    }
    if(i == 4){
        var email = document.getElementById("email").value;
        var no = document.getElementById("no").value;
        if(!email){
            toastr.warning("Email is required.");
        }
        else{
            if(!no){
                toastr.warning("Phone number is required.");
            }
            else{
                connext(i);
            }
        }
    }
    if(i == 5){
        var program = document.getElementById("program").value;
        if(!program){
            toastr.warning("Please select program");
        }
        else{
            connext(i);
        }
    }
}
function connext(i){
    var n = document.getElementById(i);
    var c = document.getElementById(i-1);
    c.style = "display: none;";
    n.style = "display: none";
    n.style = "display: ";
}
function prev(i){
    var n = document.getElementById(i);
    var c = document.getElementById(i+1);
    c.style = "display: none;";
    n.style = "display: !important";
}
