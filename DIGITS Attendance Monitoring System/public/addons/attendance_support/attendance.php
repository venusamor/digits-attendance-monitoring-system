<?php
    $data = $_POST['data'];

    $myData = explode('_', $data);
    
    //get date and time from array
    $date = $myData[1];
    $time = $myData[2];

    //add 0 to month and day
    $monthtemp = "";
    $daytemp = "";
    $datelist = explode('-', $date);
    
    if($datelist[1] < 10){
        $monthtemp = "0".$datelist[1];
        $datelist[1] = $monthtemp;
    }
    if($datelist[2] < 10){
        $daytemp = "0".$datelist[2];
        $datelist[2] = $daytemp;
    }

    //restore date format
    $date = $datelist[0].'-'.$datelist[1].'-'.$datelist[2];
    
    header("location: ../../attendance?sid=".$myData[0]."&d=".$date."&t=".$time."&ampm=".$myData[3]."&eid=".$myData[4]);
?>