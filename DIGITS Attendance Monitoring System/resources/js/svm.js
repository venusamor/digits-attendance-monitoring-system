fetch('../../addons/assets/root/JSON/svm.json')
    .then(res => res.json())
    .then(res => train(res))

function train(data){
    var mega = data.length;
    var av_max = 0;
    var matrix_p = 0;
    for (let index = 0; index <= data.length - 1; index++) {
        av_max = av_max + data[index];
    }
    matrix_p = av_max / mega;
    
    var ml = require('machine_learning');
    var x = [[10],[20],[30],[40],[50],[60],[70],[80],[90],[100]];
    var y =  [-1, -1, -1, -1, -1, -1, 1, 1, 1, 1];

    var svm = new ml.SVM({
        x : x,
        y : y
    });
    svm.train({
        C : 1.1,
        tol : 1e-5,
        max_passes : 20,
        alpha_tol : 1e-5,
        kernel : { type: "polynomial", c: 100, d: 1}
    });
    var o_rem = svm.predict([matrix_p]);

    var per = document.getElementById('per');
    per.innerText = "Percentage: " + parseFloat(matrix_p).toFixed(2) + "%";
    
    var rem = document.getElementById('rem');
    if(o_rem == 1){
        rem.innerText = "Remark: Employable";
    }
    else{
        rem.innerText = "Remark: Less Employable";

    }
}
