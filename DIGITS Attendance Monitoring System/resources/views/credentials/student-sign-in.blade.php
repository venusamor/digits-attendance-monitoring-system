<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DIGITS Attendance Monitoring System</title>
    <?php include "addons/includes/master-css.php"?>
    <?php include "addons/includes/master-js.php"?>
    <?php include "addons/includes/student-css.php"?>
</head>
    <body>
        <div>
            <div class="row">
                <div class="cred-con-in">
                    <div class="col pt-5 pr-4 pl-4">
                        <div class="cred-header">
                            <img src="../addons/assets/img/icon1.png" alt="">
                            <div>
                                <h2>DIGITS</h2>
                                <h3>Attendance Monitoring System</h3>
                            </div>
                        </div>
                        <form action="{{ route('student-signup.show','0') }}" method="get">
                            @csrf
                            @method('get')
                            <h5><span class="fa fa-user"></span> STUDENT SIGN IN</h5>
                            <div class="form-group">
                                <input type="text" placeholder="Username" name="username" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="password" placeholder="Password" name="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary col"><span class="fa fa-sign-in"></span> Sign In</button>
                            </div>
                        </form>
                        <div class="col" style="text-align: right;">
                            <h6>Don't have an account? <a href="{{ url('student-signup') }}">Sign Up</a></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "addons/includes/master-js.php"?>
        <script>
            <?php
                if(session()->has('student-message')){
                    echo session("student-message")[0].'("'.session("student-message")[1].'")';
                    session()->forget('student-message'); 
                }
            ?>
        </script>
    </body>
</html>