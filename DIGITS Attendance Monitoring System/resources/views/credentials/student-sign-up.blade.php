<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DIGITS Attendance Monitoring System</title>
    <?php include "addons/includes/master-css.php"?>
    <?php include "addons/includes/master-js.php"?>
    <?php include "addons/includes/student-css.php"?>
</head>
    <body>
        <div>
            <div class="row">
                <div class="cred-con-up">
                    <div class="col pt-5 pr-4 pl-4 p-5">
                        <div class="cred-header">
                            <img src="../addons/assets/img/icon1.png" alt="">
                            <div>
                                <h2>DIGITS</h2>
                                <h3>Attendance Monitoring System</h3>
                            </div>
                        </div>
                        <form action="addons/qrcode_support/qrcode.php" method="post">
                            @csrf 
                            @method('post')
                            <h5><span class="fa fa-user"></span> STUDENT SIGN UP</h5>
                            <div class="form-group">
                                <label for="">Student No. (This will serve as username.)</label>
                                <input required type="number" name="stuid" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="">Last Name</label>
                                        <input required type="text" name="lname" class="form-control">
                                    </div>
                                    <div class="col-md-5">
                                        <label for="">First Name</label>
                                        <input required type="text" name="fname" class="form-control">
                                    </div>
                                    <div class="col-md-2">
                                        <label for="">MI</label>
                                        <input required type="text" name="mi" class="form-control">    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Age</label>
                                        <input required type="number" name="age" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Birth Date</label>
                                        <input required type="date" name="bday" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Address</label>
                                <input required type="text" name="address" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input required type="email" name="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Year</label>
                                        <select required name="year" id="" class="form-control">
                                            @foreach($year as $y)
                                                <option value="{{ $y->yname }}">{{ $y->yname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Section</label>
                                        <select required name="section" id="" class="form-control">
                                            @foreach($section as $s)
                                                <option value="{{ $s->sname }}">{{ $s->sname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label for="">Create New Password</label>
                                <input required type="password" name="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Retype Password</label>
                                <input required type="password" name="repassword" class="form-control">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success col"><span class="fa fa-sign-in"></span> Submit</button>
                            </div>
                        </form>
                        <div class="col" style="text-align: right;">
                            <h6>Already have an account? <a href="{{ url('student-signin') }}">Sign In</a></h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "addons/includes/master-js.php"?>
        <script>
            <?php
                if(session()->has('student-message')){
                    echo session("student-message")[0].'("'.session("student-message")[1].'")';
                    session()->forget('student-message'); 
                }
            ?>
        </script>
    </body>
</html>