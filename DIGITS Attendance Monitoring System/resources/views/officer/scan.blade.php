<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DIGITS Attendance Monitoring System</title>
    <?php include "addons/includes/master-css.php"?>
    <?php include "addons/includes/master-js.php"?>
    <?php include "addons/includes/officer-css.php"?>
</head>
    <body>
        <div class="table-container">
            <div class="row card-list">
                <div class="modal-header col">
                    <h4><span class="fa fa-qrcode"></span> {{ $event[0]->name }} Attendance</h4>
                    <input id="eid" type="hidden" value="{{ $event[0]->eid }}">
                    <div class="timeDate row">
                        <h5 id="date" class="mr-3">May 25, 2019 Thursday</h5>|              
                        <h5 id="time" class="ml-3" style="text-transform: uppercase;">17:56 PM</h5>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row card-list" style="text-align: center;">
                <div class="col">
                    <input type="hidden" name="" id="d">
                    <input type="hidden" name="" id="t">    
                    <input type="hidden" name="" id="ampm">    
                    <div class="scan-container mt-3">
                       <video id="preview"></video> 
                    </div>
                </div>
            </div>
            <hr>
            <div class="row card-list p-3" id="profileres">
                <div class="col-md-3">
                    <div class="attendance-card mb-4">
                        <img src="../addons/assets/root/profiles/default.png" alt="">
                        <div>
                            <h6><b>Student No.: </b>xxxxxxx</h6>
                            <h6><b>Name: </b>...</h6>
                            <h6><b>Year & Section: </b>...</h6>
                        </div>
                    </div>    
                </div>
            </div>
        </div>
        <?php include "addons/includes/master-js.php"?>
        <script>
            setInterval(startTime(), 1000);

            function startTime() {
                // declare array for month
                var monthList = ["Null","January","Febuary","March","April","May","June","July","August","September","October","November","December"]
                // create new object for Date
                var today = new Date();
                // format hour to 24 hour format
                today.toLocaleString('en-US', { hour: 'numeric', hour12: true });
                // get time
                var hours = today.getHours();
                var minutes = today.getMinutes();
                var seconds = today.getSeconds();
                // get am or pm
                var ampm = hours >= 12 ? 'pm' : 'am';
                // get hour for 12 hour format
                hours = hours % 12;
                hours = hours ? hours : 12;
                // get month
                var month = today.getMonth() + 1;
                var day = today.getDate();
                var year = today.getFullYear();
                // add 0 to time if time < 10
                minutes = checkTime(minutes);
                seconds = checkTime(seconds);
                // display date and time
                document.getElementById('time').innerHTML = hours + ":" + minutes + ":" + seconds + " " + ampm;
                document.getElementById('date').innerHTML = monthList[month] + " " + day + " ," + year;
                // ready time for attendance
                document.getElementById('t').value = hours + ":" + minutes + ":" + seconds;
                document.getElementById('d').value = year + "-" + month + "-" + day;
                document.getElementById('ampm').value = ampm;
                // set timeout after execution
                var t = setTimeout(startTime, 500);
            }
            function checkTime(i) {
                if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
                return i;
            }


            /**
             * Qrcode scanning and attendance process
             */
            function sub(){
                var t = document.getElementById('t').value;
                var d = document.getElementById('d').value;
                var ampm = document.getElementById('ampm').value;

                window.location.href = "../" + "?date=" + d + "&time=" + t + "&ampm=" + ampm;
            }
            Instascan.Camera.getCameras().then(function (cameras){
                if(cameras.length>0){
                    scanner.start(cameras[0]);
                    /*$('[name="options"]').on('change',function(){
                        if($(this).val()==1){
                            if(cameras[0]!=""){
                                scanner.start(cameras[0]);
                            }else{
                                alert('No Front camera found!');
                            }
                        }else if($(this).val()==2){
                            if(cameras[1]!=""){
                                scanner.start(cameras[1]);
                            }else{
                                alert('No Back camera found!');
                            }
                        }
                    });*/
                }else{
                    console.error('No cameras found.');
                    alert('No cameras found.');
                }
            }).catch(function(e){
                console.error(e);
                console.log(e);
            });
            var scanner = new Instascan.Scanner({ video: document.getElementById('preview'), scanPeriod: 5, mirror: true });
            scanner.addListener('scan',function(content){
                console.log(content)
                var myArray = content.split(".");
                if(myArray[0] != "DIGITS"){
                    toastr.options = {
                        "closeButton": true,
                        "newestOnTop": true,
                        "progressBar": true,
                        "positionClass": "toast-bottom-right",
                    }
                    var audio = new Audio('../addons/assets/sound/wapking.mp3');
                    audio.play();
                    toastr.warning("Invalid QRcode");
                }else{
                    console.log(content);
                    var sid = myArray[2];
                    fetch('../api/getscanresult')
                    .then(response => response.json())
                    .then(response => displayProfile(response, sid))
                    .catch(err => console.log('Request Failed', err));
                }
                
            });


            function displayProfile(data, sid){
                console.log(data[1])
                var index = data[1];
                var i = 0;

                while(i <= index){
                    if(sid == data[0][i].sid){
                        var a = document.getElementById("profileres");
                        a.insertAdjacentHTML('afterbegin', '<div class="col-md-3 mb-4"><div class="attendance-card"><img src="../addons/assets/root/profiles/'+ data[0][i].profile +'" alt=""><div><h6><b>Student No.: </b>'+ data[0][i].stuid +'</h6><h6><b>Name: </b>'+ data[0][i].lname + ', '+ data[0][i].fname + ' ' + data[0][i].mi + '.' +'</h6><h6><b>Yr. & Sec.: </b>'+ data[0][i].year + ' - ' + data[0][i].section +'</h6></div></div></div>');
                        var audio = new Audio('../addons/assets/sound/wapking.mp3');
                        audio.play();
                        attendance(sid);
                    }
                    i++;
                }
            }
            function attendance(sid){
                toastr.options = {
                    "closeButton": true,
                    "newestOnTop": true,
                    "progressBar": true,
                    "positionClass": "toast-bottom-left",
                }
                var eid = document.getElementById('eid').value;
                var t = document.getElementById('t').value;
                var d = document.getElementById('d').value;
                var ampm = document.getElementById('ampm').value;
                var data = sid + "_" + d + "_" + t + '_' + ampm + '_' + eid;
                fetch('../addons/attendance_support/attendance.php', {
                    method: 'POST',
                    body: new URLSearchParams('data=' + data)
                })
                .then(res => res.json())
                .then(res => popup(res))
                .catch(e => toastr.warning("Invalid QR Code"))
            }
            function popup(res){
                toastr.options = {
                    "closeButton": true,
                    "newestOnTop": true,
                    "progressBar": true,
                    "positionClass": "toast-bottom-right",
                }
                var type = res[0];
                var message = res[1];

                if(type == "warning"){
                    toastr.warning(message);
                }
                if(type == "info"){
                    toastr.info(message);
                }
                if(type == "success"){
                    toastr.success(message);
                }
                if(type == "error"){
                    toastr.error(message);
                }
            }
        </script>
    </body>
    
</html>