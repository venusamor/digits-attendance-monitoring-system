<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DIGITS Attendance Monitoring System</title>
    <?php include "addons/includes/master-css.php"?>
    <?php include "addons/includes/master-js.php"?>
    <?php include "addons/includes/officer-css.php"?>
</head>
    <body>
        <div class="table-container">
            <div class="card-list col" style="width: 700px; margin: auto;">
                <div class="row col" style="display: flex; justify-content: space-between;">
                    <h5><span class="fa fa-calendar"></span> Attendance Reports</h5>
                    <button class="btn btn-primary btn-sm" onclick="printCard('card')"><span class="fa fa-print"></span> Print</button>
                </div>
                <hr>
                <div id="card">
                    <div class="row">
                        <div class="attendance-card col" style="box-shadow: none !important;">
                            <img src="../addons/assets/root/profiles/{{ $info[0]->profile }}" style="width: 100px !important; height: 110px !important;" alt="">
                            <div>
                                <h6><b>Student No.: </b>{{ $info[0]->stuid }}</h6>
                                <h6><b>Name: </b>{{ $info[0]->lname }}, {{ $info[0]->fname }} {{ $info[0]->mi }}.</h6>
                                <h6><b>Year & Section: </b>{{ $info[0]->year }} - {{ $info[0]->section }}</h6>
                                <h6><b>Number of Days Absent: </b>{{ $sanction }} Day/s</h6>
                            </div>
                        </div> 
                    </div> 
                    <hr>
                    <div class="row">
                        <table>
                            <thead>
                                <tr>
                                    <td>Event Name</td>
                                    <td>Date</td>
                                    <td>Morning</td>
                                    <td>Afternoon</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($studentattendance as $sa)
                                    @foreach($event as $e)
                                        @if($e->eid == $sa->eid)
                                        <tr>
                                            <td><h6>{{ $e->name }}</h6></td>
                                            <td><h6>{{ date('M d, Y', strtotime($e->date)) }}</h6></td>
                                            <td>
                                                <h6><b>Time In: </b>{{ $sa->min }}</h6>
                                                <h6><b>Time Out: </b>{{ $sa->mout }}</h6>
                                            </td>
                                            <td>
                                                <h6><b>Time In: </b>{{ $sa->ain }}</h6>
                                                <h6><b>Time Out: </b>{{ $sa->aout }}</h6>
                                            </td>
                                        </tr>
                                        @endif
                                    @endforeach
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                
            </div>
        </div>
        <?php include "addons/includes/master-js.php"?>
        <script>
            <?php
                if(session()->has('officer-message')){
                    echo session("officer-message")[0].'("'.session("officer-message")[1].'")';
                    session()->forget('officer-message'); 
                }
            ?>
        </script>
    </body>
    
</html>