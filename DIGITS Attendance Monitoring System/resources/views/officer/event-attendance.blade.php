<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DIGITS Attendance Monitoring System</title>
    <?php include "addons/includes/master-css.php"?>
    <?php include "addons/includes/master-js.php"?>
    <?php include "addons/includes/officer-css.php"?>
</head>
    <body>
        <?php include "addons/navigations/officer-sidebar.php"?>
        <div class="display-container">
            <div class="table-container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="card-list">
                            <div class="modal-header">
                                <h6><span class="fa fa-list"></span> School Events List</h6>
                            </div>
                            <table class="table-hover">
                                <tbody>
                                    @foreach($event as $e)
                                        @if($eventselected[0]->eid == $e->eid)
                                            <tr>
                                                <td style="display: flex; justify-content: space-between;" class="col">
                                                    <a class="event-list" href="{{ route('officer-eventattendance.show',$e->id) }}"><b>{{ $e->name }}</b></a>
                                                    <a class="btn btn-primary btn-sm" href="{{ route('officer-scan.show',$e->eid) }}"><span class="fa fa-qrcode"></span> Scan</a>
                                                </td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td style="display: flex; justify-content: space-between;" class="col">
                                                    <a class="event-list" href="{{ route('officer-eventattendance.show',$e->id) }}"><b>{{ $e->name }}</b></a>
                                                    <a class="btn btn-secondary btn-sm" href="{{ route('officer-scan.show',$e->eid) }}"><span class="fa fa-qrcode"></span> Scan</a>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="card-list">
                            <div class="modal-header">
                                <h6><span class="fa fa-calendar"></span> {{ session('event-name') }} Attendance List</h6>
                            </div>
                            <table>
                                <thead>
                                    <tr>
                                        <td>Profile</td>
                                        <td>Student Info</td>
                                        <td>Morning</td>
                                        <td>Afternoon</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($eventselected as $es)
                                        @foreach($studentattendance as $sa)
                                            @if($es->eid == $sa->eid)
                                                @foreach($student as $st)
                                                    @if($sa->sid == $st->sid)
                                                        <tr>
                                                            <td style="text-align: center;">
                                                                <img class="student-profile" src="../addons/assets/root/profiles/{{ $st->profile }}" alt="">
                                                            </td>
                                                            <td>
                                                                <h6><b>Student No.: </b>{{ $st->stuid }}</h6>
                                                                <h6><b>Name: </b>{{ $st->lname }}, {{ $st->fname }} {{ $st->mi }}.</h6>
                                                            </td>
                                                            <td>
                                                                <h6><b>In: <span style="color: green; padding: 3px;">{{ $sa->min }}</span></b></h6>
                                                                <h6><b>Out: <span style="color: blue; padding: 3px;">{{ $sa->mout }}</span></b></h6>
                                                            </td>
                                                            <td>
                                                                <h6><b>In: <span style="color: green; padding: 3px;">{{ $sa->ain }}</span></b></h6>
                                                                <h6><b>Out: <span style="color: blue; padding: 3px;">{{ $sa->aout }}</span></b></h6>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                        @endforeach
                                    @endforeach
                                </tbody>
                            </table>    
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="accountmanagement">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <form action="{{ route('officer-accountmanagement.update',$info[0]->id) }}" method="post">
                            @csrf 
                            @method('put')
                            <h5><span class="fa fa-gears"></span> Account Management</h5>
                            <div class="form-group">
                                <label for="">Student No.</label>
                                <input required readonly value="{{ $info[0]->stuid }}" type="number" name="stuid" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="">Last Name</label>
                                        <input required value="{{ $info[0]->lname }}" type="text" name="lname" class="form-control">
                                    </div>
                                    <div class="col-md-5">
                                        <label for="">First Name</label>
                                        <input required value="{{ $info[0]->fname }}" type="text" name="fname" class="form-control">
                                    </div>
                                    <div class="col-md-2">
                                        <label for="">MI</label>
                                        <input required value="{{ $info[0]->mi }}" type="text" name="mi" class="form-control">    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Age</label>
                                        <input required value="{{ $info[0]->age }}" type="number" name="age" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Birth Date</label>
                                        <input required value="{{ $info[0]->birthdate }}" type="date" name="bday" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Address</label>
                                <input required value="{{ $info[0]->address }}" type="text" name="address" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input required value="{{ $info[0]->email }}" type="email" name="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Year</label>
                                        <select required name="year" id="" class="form-control">
                                            <option  value="{{ $info[0]->year }}">{{ $info[0]->year }}</option>
                                            @foreach($year as $y)
                                                <option value="{{ $y->yname }}">{{ $y->yname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Section</label>
                                        <select required name="section" id="" class="form-control">
                                        <option  value="{{ $info[0]->section }}">{{ $info[0]->section }}</option>
                                            @foreach($section as $s)
                                                <option value="{{ $s->sname }}">{{ $s->sname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Position</label>
                                <select required name="position" id="" class="form-control">
                                    <option  value="{{ $info[0]->position }}">{{ $info[0]->position }}</option>
                                    @foreach($position as $p)
                                        <option value="{{ $p->pname }}">{{ $p->pname }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary col"><span class="fa fa-save"></span> Save Changes</button>
                            </div>
                        </form>
                        <hr>
                        <form action="{{ route('officer-signup.update',$info[0]->oid) }}" method="post">
                            @csrf 
                            @method('put')
                            <div class="form-group">
                                <label for="">Create New Password</label>
                                <input required type="password" name="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Retype Password</label>
                                <input required type="password" name="repassword" class="form-control">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary col"><span class="fa fa-save"></span> Save Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include "addons/includes/master-js.php"?>
        <script>
            <?php
                if(session()->has('officer-message')){
                    echo session("officer-message")[0].'("'.session("officer-message")[1].'")';
                    session()->forget('officer-message'); 
                }
            ?>
        </script>
    </body>
    
</html>