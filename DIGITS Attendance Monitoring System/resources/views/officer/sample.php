<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OJT Tabulation and Attendance Management System</title>
    <?php include "addons/includes/master-css.php"?>
    <?php include "addons/includes/master-js.php"?>
    <?php include "addons/includes/officer-css.php"?>
</head>
    <body>
        <?php include "addons/navigations/officer-sidebar.php"?>
        <div class="display-container">
            <div class="table-container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="card-list">
                            <div class="modal-header">
                                <h6><span class="fa fa-search"></span> Search Student ID</h6>
                            </div>
                            <form action="">
                                <div class="form-group mt-4">
                                    <div class="btn-group col" style="padding: 0px; margin: 0px;">
                                        <input type="number" class="form-control">
                                        <button class="btn btn-secondary"><span class="fa fa-search"></span></button>
                                    </div>   
                                </div>    
                            </form>
                        </div>
                        <div class="card-list mt-4">
                            <div class="modal-header">
                                <h6><span class="fa fa-edit"></span> Select Year & Section</h6>
                            </div>
                            <form action="{{ route('officer-studentlist.show','0') }}" method="get">
                                @csrf 
                                @method('get')
                                <div class="form-group">
                                    <label for="">Year</label>
                                    <select name="year" id="" class="form-control">
                                        <option value="{{ session('ys')[0] }}">{{ session('ys')[0] }}</option>
                                        @foreach($year as $y)
                                            <option value="{{ $y->yname }}">{{ $y->yname }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">Section</label>
                                    <select name="section" id="" class="form-control">
                                        <option value="{{ session('ys')[1] }}">{{ session('ys')[1] }}</option>
                                        @foreach($section as $s)
                                            <option value="{{ $s->sname }}">{{ $s->sname }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary col btn-sm"><span class="fa fa-list"></span> View List</button>
                                </div>
                            </form>
                        </div>
                        
                    </div>
                    <div class="col-md-8">
                        <div class="card-list">
                            <div class="modal-header">
                                <h6><span class="fa fa-list"></span> Student List</h6>
                            </div>
                            <table>
                                <thead>
                                    <tr>
                                        <td>Profile</td>
                                        <td>Student Info</td>
                                        <td>Year & Section</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($student as $st)
                                        @if(session('ys')[1] == "All")
                                            <tr>
                                                <td style="text-align: center;">
                                                    <img class="student-profile" src="../addons/assets/root/profiles/{{ $st->profile }}" alt="">
                                                </td>
                                                <td>
                                                    <h6><b>Student No.: </b>{{ $st->stuid }}</h6>
                                                    <h6><b>Name: </b>{{ $st->lname }}, {{ $st->fname }} {{ $st->mi }}.</h6>
                                                    <h6><b>Address: </b>{{ $st->address }}</h6>
                                                    <h6><b>Email: </b>{{ $st->email }}</h6>
                                                </td>
                                                <td>
                                                    <h6><b>Year: </b>{{ $st->year }}</h6>
                                                    <h6><b>Section: </b>{{ $st->section }}</h6>
                                                </td>
                                                <td style="width: 150px; text-align: center;">
                                                    <div class="dropdown">
                                                        <button class="btn btn-success btn-sm col-md-8 dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                Action
                                                        </button>
                                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                            <a href="#" class="dropdown-item btn-primary"><span class="fa fa-qrcode"></span> QR Code</a>
                                                            <a href="#" class="dropdown-item btn-success"><span class="fa fa-list"></span> Report</a>
                                                            <a href="#" data-toggle="modal" data-target="#viewstudent{{ $st->id }}" class="dropdown-item btn-primary"><span class="fa fa-book"></span> View</a>
                                                            <a href="#" class="dropdown-item btn-danger"><span class="fa fa-remove"></span> Remove</a>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            
                                            <div class="modal fade" id="viewstudent{{ $st->id }}">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h6><span class="fa fa-user"></span> Student Information</h6>
                                                            <h5><a data-dismiss="modal">&times;</a></h5>
                                                        </div>
                                                        <form action="">
                                                            <div class="modal-body">
                                                                <div class="form-group view-student-profile">
                                                                    <img class="cover" src="../addons/assets/img/table1.png" alt="">
                                                                    <img class="profile" src="../addons/assets/root/profiles/{{ $st->profile }}" alt="">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">Student No. </label>
                                                                    <input readonly value="{{ $st->stuid }}" type="text" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-5" style="padding: 0px; margin: 0px;">
                                                                            <label for="">Last Name</label>
                                                                            <input readonly value="{{ $st->lname }}" type="text" class="form-control">
                                                                        </div>
                                                                        <div class="col-md-5" style="padding: 0px; margin: 0px;">
                                                                            <label for="">First Name</label>
                                                                            <input readonly value="{{ $st->fname }}" type="text" class="form-control">
                                                                        </div>
                                                                        <div class="col-md-2" style="padding: 0px; margin: 0px;">
                                                                            <label for="">MI</label>
                                                                            <input readonly value="{{ $st->mi }}" type="text" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-6" style="padding: 0px; margin: 0px;">
                                                                            <label for="">Age</label>
                                                                            <input readonly value="{{ $st->age }}" type="text" class="form-control">
                                                                        </div>
                                                                        <div class="col-md-6" style="padding: 0px; margin: 0px;">
                                                                            <label for="">Birth Date</label>
                                                                            <input readonly value="{{ date('M d, Y', strtotime($st->birthdate)) }}" type="text" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">Address</label>
                                                                    <input readonly value="{{ $st->address }}" type="text" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="">Email</label>
                                                                    <input readonly value="{{ $st->email }}" type="text" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <div class="row">
                                                                        <div class="col-md-6" style="padding: 0px; margin: 0px;">
                                                                            <label for="">Year</label>
                                                                            <input readonly value="{{ $st->year }}" type="text" class="form-control">
                                                                        </div>
                                                                        <div class="col-md-6" style="padding: 0px; margin: 0px;">
                                                                            <label for="">Section</label>
                                                                            <input readonly value="{{ $st->section }}" type="text" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>  
                                        @else
                                            @if($st->section == session('ys')[1])
                                                <tr>
                                                    <td style="text-align: center;">
                                                        <img class="student-profile" src="../addons/assets/root/profiles/{{ $st->profile }}" alt="">
                                                    </td>
                                                    <td>
                                                        <h6><b>Student No.: </b>{{ $st->stuid }}</h6>
                                                        <h6><b>Name: </b>{{ $st->lname }}, {{ $st->fname }} {{ $st->mi }}.</h6>
                                                        <h6><b>Address: </b>{{ $st->address }}</h6>
                                                        <h6><b>Email: </b>{{ $st->email }}</h6>
                                                    </td>
                                                    <td>
                                                        <h6><b>Year: </b>{{ $st->year }}</h6>
                                                        <h6><b>Section: </b>{{ $st->section }}</h6>
                                                    </td>
                                                    <td style="width: 150px; text-align: center;">
                                                        <div class="dropdown">
                                                            <button class="btn btn-success btn-sm col-md-8 dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    Action
                                                            </button>
                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                <a href="#" data-toggle="modal" data-target="#qrcode{{ $st->id }}" class="dropdown-item btn-primary"><span class="fa fa-qrcode"></span> QR Code</a>
                                                                <a href="#" class="dropdown-item btn-success"><span class="fa fa-list"></span> Report</a>
                                                                <a href="#" data-toggle="modal" data-target="#viewstudent{{ $st->id }}" class="dropdown-item btn-primary"><span class="fa fa-book"></span> View</a>
                                                                <a href="#" class="dropdown-item btn-danger"><span class="fa fa-remove"></span> Remove</a>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <div class="modal fade" id="qrcode{{ $st->id }}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h6><span class="fa fa-qrcode"></span> Student QR Code</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                <div class="modal fade" id="viewstudent{{ $st->id }}">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h6><span class="fa fa-user"></span> Student Information</h6>
                                                                <h5><a data-dismiss="modal">&times;</a></h5>
                                                            </div>
                                                            <form action="">
                                                                <div class="modal-body">
                                                                    <div class="form-group view-student-profile">
                                                                        <img class="cover" src="../addons/assets/img/table1.png" alt="">
                                                                        <img class="profile" src="../addons/assets/root/profiles/{{ $st->profile }}" alt="">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="">Student No. </label>
                                                                        <input readonly value="{{ $st->stuid }}" type="text" class="form-control">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-md-5" style="padding: 0px; margin: 0px;">
                                                                                <label for="">Last Name</label>
                                                                                <input readonly value="{{ $st->lname }}" type="text" class="form-control">
                                                                            </div>
                                                                            <div class="col-md-5" style="padding: 0px; margin: 0px;">
                                                                                <label for="">First Name</label>
                                                                                <input readonly value="{{ $st->fname }}" type="text" class="form-control">
                                                                            </div>
                                                                            <div class="col-md-2" style="padding: 0px; margin: 0px;">
                                                                                <label for="">MI</label>
                                                                                <input readonly value="{{ $st->mi }}" type="text" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-md-6" style="padding: 0px; margin: 0px;">
                                                                                <label for="">Age</label>
                                                                                <input readonly value="{{ $st->age }}" type="text" class="form-control">
                                                                            </div>
                                                                            <div class="col-md-6" style="padding: 0px; margin: 0px;">
                                                                                <label for="">Birth Date</label>
                                                                                <input readonly value="{{ date('M d, Y', strtotime($st->birthdate)) }}" type="text" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="">Address</label>
                                                                        <input readonly value="{{ $st->address }}" type="text" class="form-control">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="">Email</label>
                                                                        <input readonly value="{{ $st->email }}" type="text" class="form-control">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <div class="row">
                                                                            <div class="col-md-6" style="padding: 0px; margin: 0px;">
                                                                                <label for="">Year</label>
                                                                                <input readonly value="{{ $st->year }}" type="text" class="form-control">
                                                                            </div>
                                                                            <div class="col-md-6" style="padding: 0px; margin: 0px;">
                                                                                <label for="">Section</label>
                                                                                <input readonly value="{{ $st->section }}" type="text" class="form-control">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div> 
                                            @endif       
                                        @endif
                                    
                                    @endforeach
                                </tbody>
                            </table>            
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        <div class="modal fade" id="accountmanagement">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <form action="{{ route('officer-accountmanagement.update',$info[0]->id) }}" method="post">
                            @csrf 
                            @method('put')
                            <h5><span class="fa fa-gears"></span> Account Management</h5>
                            <div class="form-group">
                                <label for="">Student No.</label>
                                <input required readonly value="{{ $info[0]->stuid }}" type="number" name="stuid" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="">Last Name</label>
                                        <input required value="{{ $info[0]->lname }}" type="text" name="lname" class="form-control">
                                    </div>
                                    <div class="col-md-5">
                                        <label for="">First Name</label>
                                        <input required value="{{ $info[0]->fname }}" type="text" name="fname" class="form-control">
                                    </div>
                                    <div class="col-md-2">
                                        <label for="">MI</label>
                                        <input required value="{{ $info[0]->mi }}" type="text" name="mi" class="form-control">    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Age</label>
                                        <input required value="{{ $info[0]->age }}" type="number" name="age" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Birth Date</label>
                                        <input required value="{{ $info[0]->birthdate }}" type="date" name="bday" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Address</label>
                                <input required value="{{ $info[0]->address }}" type="text" name="address" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input required value="{{ $info[0]->email }}" type="email" name="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Year</label>
                                        <select required name="year" id="" class="form-control">
                                            <option  value="{{ $info[0]->year }}">{{ $info[0]->year }}</option>
                                            @foreach($year as $y)
                                                <option value="{{ $y->yname }}">{{ $y->yname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Section</label>
                                        <option  value="{{ $info[0]->ection }}">{{ $info[0]->ection }}</option>
                                        <select required name="section" id="" class="form-control">
                                            @foreach($section as $s)
                                                <option value="{{ $s->sname }}">{{ $s->sname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Position</label>
                                <select required name="position" id="" class="form-control">
                                    <option  value="{{ $info[0]->position }}">{{ $info[0]->position }}</option>
                                    @foreach($position as $p)
                                        <option value="{{ $p->pname }}">{{ $p->pname }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary col"><span class="fa fa-save"></span> Save Changes</button>
                            </div>
                        </form>
                        <hr>
                        <form action="{{ route('officer-signup.update',$info[0]->oid) }}" method="post">
                            @csrf 
                            @method('put')
                            <div class="form-group">
                                <label for="">Create New Password</label>
                                <input required type="password" name="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Retype Password</label>
                                <input required type="password" name="repassword" class="form-control">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary col"><span class="fa fa-save"></span> Save Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include "addons/includes/master-js.php"?>
    </body>
    
</html>