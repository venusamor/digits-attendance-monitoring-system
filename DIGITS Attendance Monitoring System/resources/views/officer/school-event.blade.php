<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DIGITS School Events</title>
    <?php include "addons/includes/master-css.php"?>
    <?php include "addons/includes/master-js.php"?>
    <?php include "addons/includes/officer-css.php"?>
</head>
    <body>
        <?php include "addons/navigations/officer-sidebar.php"?>
        <div class="display-container">
            <div class="table-container">
                <div class="row">
                    <div class="col-md-5">
                        <div class="card-list">
                            <div class="modal-header">
                                <h6><span class="fa fa-edit"></span> Create School Event</h6>
                            </div>
                            <form action="{{ route('officer-event.store') }}" method="post">
                                @csrf 
                                @method('post')
                                <div class="form-group">
                                    <label for="">Name</label>
                                    <input required type="text" class="form-control" name="name">
                                </div>
                                <div class="form-group">
                                    <label for="">Venue</label>
                                    <input required type="text" class="form-control" name="venue">
                                </div>
                                <div class="form-group">
                                    <label for="">Date</label>
                                    <input required type="date" class="form-control" name="date">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <button class="btn btn-primary col"><span class="fa fa-save"></span> Add Event</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="card-list">
                            <div class="modal-header">
                                <h6><span class="fa fa-list"></span> School Event List</h6>
                            </div>
                            <table>
                                <thead>
                                    <tr>
                                        <td>Event Info</td>
                                        <td>Date</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($event as $e)
                                    <tr>
                                        <td>
                                            <h6><b>Name: </b>{{ $e->name }}</h6>
                                            <h6><b>Venue: </b>{{ $e->venue }}</h6>
                                        </td>
                                        <td><h6>{{date('M d, Y', strtotime($e->date)) }}</h6></td>
                                        <td style="width: 150px; text-align: center;">
                                            <div class="dropdown">
                                                <button class="btn btn-success btn-sm col-md-8 dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                        Action
                                                </button>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a data-toggle="modal" data-target="#editevent{{ $e->id }}" class="dropdown-item btn-warning"><span class="fa fa-edit"></span> Edit</a>
                                                    <a href="{{ route('officer-event.show',$e->id) }}" type="submit" class="dropdown-item btn-danger"><span class="fa fa-remove"></span> Remove</a> 
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                    <div class="modal fade" id="editevent{{ $e->id }}">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h6><span class="fa fa-edit"></span> Edit School Event</h6>
                                                    <h5><a data-dismiss="modal">&times;</a></h5>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ route('officer-event.update',$e->id) }}" method="post">
                                                        @csrf 
                                                        @method('put')
                                                        <div class="form-group">
                                                            <label for="">Name</label>
                                                            <input required value="{{ $e->name }}" type="text" class="form-control" name="name">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Venue</label>
                                                            <input required value="{{ $e->venue }}" type="text" class="form-control" name="venue">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Date</label>
                                                            <input required value="{{ $e->date }}" type="date" class="form-control" name="date">
                                                        </div>
                                                        <hr>
                                                        <div class="form-group">
                                                            <button class="btn btn-success col"><span class="fa fa-save"></span> Save Changes</button>
                                                        </div>
                                                    </form>  
                                                </div>                    
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                </tbody>
                            </table>    
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="accountmanagement">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <form action="{{ route('officer-accountmanagement.update',$info[0]->id) }}" method="post">
                            @csrf 
                            @method('put')
                            <h5><span class="fa fa-gears"></span> Account Management</h5>
                            <div class="form-group">
                                <label for="">Student No.</label>
                                <input required readonly value="{{ $info[0]->stuid }}" type="number" name="stuid" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="">Last Name</label>
                                        <input required value="{{ $info[0]->lname }}" type="text" name="lname" class="form-control">
                                    </div>
                                    <div class="col-md-5">
                                        <label for="">First Name</label>
                                        <input required value="{{ $info[0]->fname }}" type="text" name="fname" class="form-control">
                                    </div>
                                    <div class="col-md-2">
                                        <label for="">MI</label>
                                        <input required value="{{ $info[0]->mi }}" type="text" name="mi" class="form-control">    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Age</label>
                                        <input required value="{{ $info[0]->age }}" type="number" name="age" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Birth Date</label>
                                        <input required value="{{ $info[0]->birthdate }}" type="date" name="bday" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Address</label>
                                <input required value="{{ $info[0]->address }}" type="text" name="address" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input required value="{{ $info[0]->email }}" type="email" name="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Year</label>
                                        <select required name="year" id="" class="form-control">
                                            <option  value="{{ $info[0]->year }}">{{ $info[0]->year }}</option>
                                            @foreach($year as $y)
                                                <option value="{{ $y->yname }}">{{ $y->yname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Section</label>
                                        <select required name="section" id="" class="form-control">
                                        <option  value="{{ $info[0]->section }}">{{ $info[0]->section }}</option>
                                            @foreach($section as $s)
                                                <option value="{{ $s->sname }}">{{ $s->sname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Position</label>
                                <select required name="position" id="" class="form-control">
                                    <option  value="{{ $info[0]->position }}">{{ $info[0]->position }}</option>
                                    @foreach($position as $p)
                                        <option value="{{ $p->pname }}">{{ $p->pname }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary col"><span class="fa fa-save"></span> Save Changes</button>
                            </div>
                        </form>
                        <hr>
                        <form action="{{ route('officer-signup.update',$info[0]->oid) }}" method="post">
                            @csrf 
                            @method('put')
                            <div class="form-group">
                                <label for="">Create New Password</label>
                                <input required type="password" name="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Retype Password</label>
                                <input required type="password" name="repassword" class="form-control">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary col"><span class="fa fa-save"></span> Save Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include "addons/includes/master-js.php"?>
        <script>
            <?php
                if(session()->has('officer-message')){
                    echo session("officer-message")[0].'("'.session("officer-message")[1].'")';
                    session()->forget('officer-message'); 
                }
            ?>
        </script>
    </body>
    
</html>