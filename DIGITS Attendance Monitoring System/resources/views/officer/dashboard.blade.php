<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DIGITS Attendance Monitoring System</title>
    <?php include "addons/includes/master-css.php"?>
    <?php include "addons/includes/master-js.php"?>
    <?php include "addons/includes/officer-css.php"?>
</head>
    <body>
        <?php include "addons/navigations/officer-sidebar.php"?>
        <div class="display-container">
            <div class="table-container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="con-a modal-header card-list">
                            <div>
                                <h5>{{ $s }}</h5>
                                <h6>Total Number of Students</h6>
                            </div>
                            <span class="fa fa-users"></span>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="con-b modal-header card-list">
                            <div>
                                <h5>{{ $o }}</h5>
                                <h6>Number of Officers</h6>
                            </div>
                            <span class="fa fa-users"></span>
                        </div>
                    </div>
                    <div class="col-md-4 ">
                        <div class="con-c modal-header card-list">
                            <div>
                                <h5>{{ $e }}</h5>
                                <h6>Events</h6>
                            </div>
                            <span class="fa fa-flag"></span>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row mb-2  pl-4 pr-4 card-list">
                    <canvas id="myChart" width="400" height="120"></canvas>
                </div>
                <hr>
                <div class="row">
                    <div class="col-md-5">
                        <div class="card-list">
                            <div class="modal-header">
                                <h6><span class="fa fa-edit"></span> Make An Announcement</h6>
                            </div>
                            <form action="{{ route('officer-announcement.store') }}" method="post">
                                @csrf 
                                @method('post')
                                <div class="form-group">
                                    <label for="">Announcer</label>
                                    <input required type="text" value="{{  $info[0]->fname }}" class="form-control" name="announcer">
                                </div>
                                <div class="form-group">
                                    <label for="">Announcement</label>
                                    <textarea required name="announcement" class="form-control" id="" cols="30" rows="5"></textarea>
                                </div>
                                <div class="form-group">
                                    <label for="">Date</label>
                                    <input required type="date" class="form-control" name="date">
                                </div>
                                <hr>
                                <div class="form-group">
                                    <button class="btn btn-primary col"><span class="fa fa-bell"></span> Announce</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div class="card-list">
                            <div class="modal-header">
                                <h6><span class="fa fa-bell"></span> Announcements</h6>
                            </div>
                            <table>
                                <thead>
                                    <tr>
                                        <td>Announcer</td>
                                        <td>Announcement</td>
                                        <td>Date</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($announcement as $an)
                                        <tr>
                                            <td><h6>{{ $an->announcer }}</h6></td>
                                            <td><textarea class="form-control" readonly cols="30" rows="1">{{ $an->announcement }}</textarea></td>
                                            <td><h6>{{ date('M d, Y', strtotime($an->date)) }}</h6></td>
                                            <td style="text-align: center; width: 120px;"><h6><a href="{{ route('officer-announcement.show',$an->id) }}" class="btn btn-sm btn-danger"><span class="fa fa-remove"></span> Remove</a></h6></td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>    
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
        <div class="modal fade" id="accountmanagement">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <form action="{{ route('officer-accountmanagement.update',$info[0]->id) }}" method="post">
                            @csrf 
                            @method('put')
                            <h5><span class="fa fa-gears"></span> Account Management</h5>
                            <div class="form-group">
                                <label for="">Student No.</label>
                                <input required readonly value="{{ $info[0]->stuid }}" type="number" name="stuid" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="">Last Name</label>
                                        <input required value="{{ $info[0]->lname }}" type="text" name="lname" class="form-control">
                                    </div>
                                    <div class="col-md-5">
                                        <label for="">First Name</label>
                                        <input required value="{{ $info[0]->fname }}" type="text" name="fname" class="form-control">
                                    </div>
                                    <div class="col-md-2">
                                        <label for="">MI</label>
                                        <input required value="{{ $info[0]->mi }}" type="text" name="mi" class="form-control">    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Age</label>
                                        <input required value="{{ $info[0]->age }}" type="number" name="age" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Birth Date</label>
                                        <input required value="{{ $info[0]->birthdate }}" type="date" name="bday" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Address</label>
                                <input required value="{{ $info[0]->address }}" type="text" name="address" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input required value="{{ $info[0]->email }}" type="email" name="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Year</label>
                                        <select required name="year" id="" class="form-control">
                                            <option  value="{{ $info[0]->year }}">{{ $info[0]->year }}</option>
                                            @foreach($year as $y)
                                                <option value="{{ $y->yname }}">{{ $y->yname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Section</label>
                                        <select required name="section" id="" class="form-control">
                                        <option  value="{{ $info[0]->section }}">{{ $info[0]->section }}</option>
                                            @foreach($section as $s)
                                                <option value="{{ $s->sname }}">{{ $s->sname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Position</label>
                                <select required name="position" id="" class="form-control">
                                    <option  value="{{ $info[0]->position }}">{{ $info[0]->position }}</option>
                                    @foreach($position as $p)
                                        <option value="{{ $p->pname }}">{{ $p->pname }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary col"><span class="fa fa-save"></span> Save Changes</button>
                            </div>
                        </form>
                        <hr>
                        <form action="{{ route('officer-signup.update',$info[0]->oid) }}" method="post">
                            @csrf 
                            @method('put')
                            <div class="form-group">
                                <label for="">Create New Password</label>
                                <input required type="password" name="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Retype Password</label>
                                <input required type="password" name="repassword" class="form-control">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary col"><span class="fa fa-save"></span> Save Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include "addons/includes/master-js.php"?>
        <script>
            var ctx = document.getElementById('myChart').getContext('2d');
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: [
                        <?php
                            foreach($section as $s1){
                                echo "'".$s1->sname."',";
                            }    
                        ?>
                    ],
                    datasets: [{
                        label: 'Number of Student',
                        data: [
                            <?php
                                foreach($section as $s1){
                                    $i =  0;
                                    foreach($student as $s2){
                                        if($s1->sname == $s2->section){
                                            $i++;
                                        }
                                    }
                                    echo $i.',';
                                }        
                            ?>
                            
                        ],
                        backgroundColor: [
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                        ],
                        borderColor: [
                            'rgba(54, 162, 235, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(54, 162, 235, 1)',
                        ],
                        borderWidth: 2
                    }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true,
                            min: 0,
                            //max: 100,
                            //stepSize: 1 // 1 - 2 - 3 ...
                        },
                        xAxes: [{
                            display: false
                        }]
                    },
                }
            });
        </script>
        <script>
            <?php
                if(session()->has('officer-message')){
                    if(session("officer-message")[0] == "welcomeMessage"){
                        echo session("officer-message")[0].'("'.session("officer-message")[1].$info[0]->fname.'")';
                        session()->forget('officer-message');   
                    }else{
                        echo session("officer-message")[0].'("'.session("officer-message")[1].'")';
                        session()->forget('officer-message');     
                    }
                }
            ?>
        </script>
    </body>
    
</html>