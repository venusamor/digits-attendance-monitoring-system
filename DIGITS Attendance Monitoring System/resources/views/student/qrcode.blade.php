<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DIGITS Attendance Monitoring System</title>
    <?php include "addons/includes/master-css.php"?>
    <?php include "addons/includes/master-js.php"?>
    <?php include "addons/includes/student-css.php"?>
</head>
    <body>
        <div class="table-container">
            <div style="width: 500px; margin: auto;">
                <div class="row card-list">
                    <div class="modal-header col">
                        <h5><span class="fa fa-qrcode"></span> QR Code</h5>
                        <div>
                            <button class="btn btn-primary btn-sm" onclick="printCard('card')"><span class="fa fa-print"></span> Print</button>
                            <a href="{{ route('student-qrcode.show',$info[0]->id) }}" class="btn btn-secondary btn-sm"><span class="fa fa-download"></span> Download QR Code</a>
                        </div>
                        
                    </div>
                </div>
                <hr>
                <div class="row card-list" style="text-align: center;">
                    <div class="card-container ml-3 col" style="text-align: center;" id="card">
                        <div class="qr-card" style="text-align: center;">
                            <div class="row">
                                <div class="col" style="text-align: center;">
                                    <h6 class="header">Republic of The Philippines</h6>
                                    <h5 class="header">Leyte Normal University</h5>
                                    <h6 class="header">Tacloban City</h6>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <img class="card-profile" src="../addons/assets/root/profiles/{{ $info[0]->profile }}" alt="">
                                </div>
                                <div class="col-md-8 pl-5" style="text-align: center;">
                                    <img class="card-logo" src="../../addons/assets/img/logo.jpg" alt="">
                                    <h6 class="mt-2">Student No.:</h6>
                                    <h5 style="margin-top: -10px; font-weight: bolder;">{{ $info[0]->stuid }}</h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col card-name">
                                    <h4 style="font-weight: bolder;">{{ $info[0]->lname }}, {{ $info[0]->fname }} {{ $info[0]->mi }}.</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col card-qrcode">
                                    <div class="circle-container col">
                                        <div class="circle">
                                            <h6>
                                                {{ $info[0]->lname[0] }}{{ $info[0]->fname[0] }}
                                            </h6>
                                        </div>
                                    </div>
                                    <img src="../addons/assets/root/qrcodes/{{ $info[0]->qrcode }}" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
            
        </div>
        <?php include "addons/includes/master-js.php"?>
        <script>
            <?php
                if(session()->has('student-message')){
                    echo session("student-message")[0].'("'.session("student-message")[1].'")';
                    session()->forget('student-message'); 
                }
            ?>
        </script>
    </body>
    
</html>