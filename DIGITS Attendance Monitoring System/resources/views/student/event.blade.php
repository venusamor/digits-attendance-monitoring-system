<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DIGITS Attendance Monitoring System</title>
    <?php include "addons/includes/master-css.php"?>
    <?php include "addons/includes/master-js.php"?>
    <?php include "addons/includes/student-css.php"?>
</head>
    <body>
        <?php include "addons/navigations/student-sidebar.php"?>
        <div class="display-container">
            <div class="table-container">
            <div class="card-list">
                    <div class="row modal-header">
                        <h6><span class="fa fa-flag"></span> School Events</h6>
                    </div>
                    <div class="row">
                        <table>
                            <thead>
                                <tr>
                                    <td>Event Name</td>
                                    <td>Date</td>
                                    <td>Venue</td>
                                    <td>Status</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($event as $e)
                                    <?php $status = "false";?>
                                    <tr>
                                        <td><h6>{{ $e->name }}</h6></td>
                                        <td><h6>{{ date('M d, Y', strtotime($e->date)) }}</h6></td>
                                        <td><h6>{{ $e->venue }}</h6></td>
                                        @foreach($studentattendance as $sa)
                                            @if($sa->eid == $e->eid)    
                                                <?php $status = "true";?>
                                            @endif
                                        @endforeach  

                                        @if($status == "true")
                                            <td style="color: green;"><h6>Attended</h6></td>
                                        @else
                                            <td style="color: red;"><h6>Absent</h6></td>
                                        @endif  
                                    </tr>
                                @endforeach
                            </tbody>
                        </table> 
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="accountmanagement">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <form action="{{ route('student-accountmanagement.update',$info[0]->id) }}" method="post">
                            @csrf 
                            @method('put')
                            <h5><span class="fa fa-gears"></span> Account Management</h5>
                            <hr>
                            <div class="form-group view-student-profile">
                                <img class="cover" src="../addons/assets/img/table1.png" alt="">
                                <img class="profile" src="../addons/assets/root/profiles/{{ $info[0]->profile }}" alt="">
                            </div> 
                            <div class="form-group">
                                <label for="">Student No.</label>
                                <input required readonly value="{{ $info[0]->stuid }}" type="number" name="stuid" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="">Last Name</label>
                                        <input required value="{{ $info[0]->lname }}" type="text" name="lname" class="form-control">
                                    </div>
                                    <div class="col-md-5">
                                        <label for="">First Name</label>
                                        <input required value="{{ $info[0]->fname }}" type="text" name="fname" class="form-control">
                                    </div>
                                    <div class="col-md-2">
                                        <label for="">MI</label>
                                        <input required value="{{ $info[0]->mi }}" type="text" name="mi" class="form-control">    
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Age</label>
                                        <input required value="{{ $info[0]->age }}" type="number" name="age" class="form-control">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Birth Date</label>
                                        <input required value="{{ $info[0]->birthdate }}" type="date" name="bday" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Address</label>
                                <input required value="{{ $info[0]->address }}" type="text" name="address" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input required value="{{ $info[0]->email }}" type="email" name="email" class="form-control">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="">Year</label>
                                        <select required name="year" id="" class="form-control">
                                            <option selected value="{{ $info[0]->year }}">{{ $info[0]->year }}</option>
                                            @foreach($year as $y)
                                                <option value="{{ $y->yname }}">{{ $y->yname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="">Section</label>
                                        <select required name="section" id="" class="form-control">
                                            <option selected  value="{{ $info[0]->section }}">{{ $info[0]->section }}</option>
                                            @foreach($section as $s)
                                                <option value="{{ $s->sname }}">{{ $s->sname }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary col"><span class="fa fa-save"></span> Save Changes</button>
                            </div>
                        </form>
                        <hr>
                        <form action="{{ route('student-signup.update',$info[0]->sid )}}" method="post">
                            @csrf 
                            @method('put')
                            <div class="form-group">
                                <label for="">Create New Password</label>
                                <input required type="password" name="password" class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="">Retype Password</label>
                                <input required type="password" name="repassword" class="form-control">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary col"><span class="fa fa-save"></span> Save Password</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="profilepicture">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <form action="../addons/file_upload_support/uploadProfile.php?id={{ $info[0]->id }}&profile={{ $info[0]->profile }}" method="POST" enctype="multipart/form-data">
                            <div class="modal-body" style="padding: 10px;">
                                <div class="form-group">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Upload</span>
                                        </div>
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" name="new_profile" id="upload" onchange="displayname(this,$(this))">
                                            <label class="custom-file-label" for="upload">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary btn-sm"><span class="fa fa-save"></span> Save</button>
                                <button data-dismiss="modal" class="btn btn-dark btn-sm"><span class="fa fa-times"></span> Cancel</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <?php include "addons/includes/master-js.php"?>
        <script>
            <?php
                if(session()->has('student-message')){
                    echo session("student-message")[0].'("'.session("student-message")[1].'")';
                    session()->forget('student-message'); 
                }
            ?>
        </script>
    </body>
    
</html>