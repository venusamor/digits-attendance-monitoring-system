<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Studentattendance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studentattendance', function (Blueprint $table) {
            $table->id();
            $table->string("eid");
            $table->string("sid");
            $table->string("min");
            $table->string("mout");
            $table->string("ain");
            $table->string("aout");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studentattendance');
    }
}
