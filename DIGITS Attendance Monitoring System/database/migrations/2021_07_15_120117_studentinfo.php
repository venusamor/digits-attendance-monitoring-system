<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Studentinfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studentinfo', function (Blueprint $table) {
            $table->id();
            $table->string("sid");
            $table->string("stuid");
            $table->string("lname");
            $table->string("fname");
            $table->string("mi");
            $table->string("age");
            $table->string("birthdate");
            $table->string("address");
            $table->string("email");
            $table->string("year");
            $table->string("section");
            $table->string("position");
            $table->string("qrcode");
            $table->string("profile");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studentinfo');
    }
}
