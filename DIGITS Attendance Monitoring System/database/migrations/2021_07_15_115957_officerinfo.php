<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Officerinfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('officerinfo', function (Blueprint $table) {
            $table->id();
            $table->string("oid");
            $table->string("stuid");
            $table->string("lname");
            $table->string("fname");
            $table->string("mi");
            $table->string("age");
            $table->string("birthdate");
            $table->string("address");
            $table->string("email");
            $table->string("year");
            $table->string("section");
            $table->string("position");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('officerinfo');
    }
}
