<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SectionYearSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Populate Year
         */
        $year = [
            '1st Year',
            '2nd Year',
            '3rd Year',
            '4th Year',
            'Others',
        ];
        $ycount = count($year);
        $i = 0;
        while($i < $ycount){
            DB::table('year')->insert([
                'yname' => $year[$i],
            ]);
            $i++;
        }

        /**
         * Populate Section
         */

        $section = [
            'AI11',
            'AI12',
            'AI13',
            'AI14',
            'AI15',
            'AI21',
            'AI22',
            'AI23',
            'AI24',
            'AI25',
            'AI31',
            'AI32',
            'AI33',
            'AI34',
            'AI35',
            'AI41',
            'AI42',
            'AI43',
            'AI44',
            'AI45',
            'Others',
        ];
        $scount = count($section);
        $i = 0;
        while($i < $scount){
            DB::table('section')->insert([
                'sname' => $section[$i],
            ]);
            $i++;
        }
    }
}
