<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class PositionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Populate Position
         */

        $position = [
            'President',
            'Vice-President',
            'Secretary',
            'Auditor',
            '1st President',
            '2nd President',
            '3rd President',
            '4th President',
            'Representative',
        ];
        $pcount = count($position);
        $i = 0;
        while($i < $pcount){
            DB::table('position')->insert([
                'pname' => $position[$i],
            ]);
            $i++;
        }
    }
}
